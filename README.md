CSS − TD1 : Les bases du CSS
============================

Exo 1
-----

En utilisant le HTML `index.html` fourni (ou celui produit lors de votre TD 1
de HTML), réaliser le [design suivant](td1-exo1.png) à l'aide d'une feuille de
style CSS.

**La seule modification à apporter au fichier HTML est la balise qui le lie
  avec le HTML**.

### Aides :

- Nous exprimerons pour ce TD toutes les dimensions en pixels (*px*)
- Vous pouvez utiliser la *pipette* du logiciel *gimp* pour connaitre les codes
des couleurs employées (indice, il est rare de croiser du véritable noir...)
- Les images nécessaires sont dans `img/`

Exo 2
-----

En utilisant les propriétés *background-** (à vous de chercher) et les
*pseudo-classes* (vues en cours), améliorez encore votre CSS **sans toucher au
HTML** pour obtenir [le résultat suivant](td1-exo2.png).

### Aides :

- Ne pas modifier le fichier image fourni pour le fond du titre
- Les éléments modifiés par rapport à l'exo 1 sont :
  - Titre principal ;
  - couleur des liens (ils doivent avoir la même couleur qu'ils aient été
  visités ou non) ;
  - numéros des titres de niveau 2.
